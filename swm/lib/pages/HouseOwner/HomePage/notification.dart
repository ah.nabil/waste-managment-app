// ignore_for_file: library_private_types_in_public_api, body_might_complete_normally_nullable

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  late Stream<QuerySnapshot> _notificationsStream;
  late String userArea = '';

  @override
  void initState() {
    super.initState();
    final currentUser = FirebaseAuth.instance.currentUser;
    getUserArea(
        currentUser?.uid); // Pass the user's UID to retrieve the area name
  }

  void getUserArea(String? userId) {
    FirebaseFirestore.instance
        .collection('users')
        .doc(userId)
        .get()
        .then((snapshot) {
      if (snapshot.exists) {
        setState(() {
          userArea = snapshot['area'];
        });
      }
    });
    _notificationsStream = FirebaseFirestore.instance
        .collection('alerts')
        .orderBy('date', descending: true)
        .snapshots();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Notifications'),
        backgroundColor: Colors.green,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: _notificationsStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            final List<QueryDocumentSnapshot> notifications =
                snapshot.data!.docs;

            if (notifications.isEmpty) {
              return Center(
                child: Text(
                  'No notifications found.',
                  style: GoogleFonts.poppins(fontSize: 18),
                ),
              );
            }

            return ListView.builder(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
              itemCount: notifications.length,
              itemBuilder: (BuildContext context, int index) {
                final notification = notifications[index];
                if (notification['area'] == userArea) {
                  final String title = notification['time'] ?? '';
                  final String subtitle = notification['title'] ?? '';
                  final String date = notification['date'] ?? '';
                  bool isRead = notification['isRead'] ?? false;

                  return NotificationCard(
                    title: title,
                    subtitle: subtitle,
                    date: date,
                    isRead: isRead,
                    onMarkAsRead: () {
                      // Update the 'isRead' field in the database
                      FirebaseFirestore.instance
                          .collection('alerts')
                          .doc(notification.id)
                          .update({'isRead': true});

                      setState(() {
                        isRead = true;
                      });
                    },
                  );
                }
              },
            );
          }

          if (snapshot.hasError) {
            return Center(
              child: Text(
                'Failed to fetch notifications.',
                style: GoogleFonts.poppins(fontSize: 18),
              ),
            );
          }

          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class NotificationCard extends StatelessWidget {
  final String title;
  final String subtitle;
  final String date;
  final bool isRead;
  final VoidCallback onMarkAsRead;

  const NotificationCard({
    super.key,
    required this.title,
    required this.subtitle,
    required this.date,
    this.isRead = false,
    required this.onMarkAsRead,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      color: isRead ? Colors.white : Colors.green[50],
      child: ListTile(
        onTap: onMarkAsRead,
        title: Text(
          title,
          style: GoogleFonts.poppins(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: isRead ? Colors.black : Colors.green,
          ),
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 8),
            Text(
              subtitle,
              style: GoogleFonts.poppins(
                fontSize: 16,
                color: isRead ? Colors.black87 : Colors.green,
              ),
            ),
            const SizedBox(height: 4),
            Text(
              'Date: $date',
              style: GoogleFonts.poppins(
                fontSize: 14,
                color: isRead ? Colors.black54 : Colors.green,
              ),
            ),
          ],
        ),
        trailing: Icon(
          Icons.circle,
          color: isRead ? Colors.grey : Colors.green,
          size: 12,
        ),
      ),
    );
  }
}
