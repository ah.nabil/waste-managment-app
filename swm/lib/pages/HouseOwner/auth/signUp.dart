// ignore_for_file: use_build_context_synchronously, prefer_const_constructors, library_private_types_in_public_api, file_names, curly_braces_in_flow_control_structures, prefer_const_literals_to_create_immutables
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:swm/component/text_field.dart';
import 'package:swm/pages/HouseOwner/HomePage/home_screen_hw.dart';

import '../../../component/button.dart';

class SignUpHW extends StatefulWidget {
  const SignUpHW({Key? key}) : super(key: key);

  @override
  _SignUpHWState createState() => _SignUpHWState();
}

class _SignUpHWState extends State<SignUpHW> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  String _selectedWard = '';
  String _selectedArea = '';

  final TextEditingController _streetController = TextEditingController();
  final TextEditingController _houseNoController = TextEditingController();
  final TextEditingController _floorController = TextEditingController();
  final TextEditingController _apartmentController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  final Map<String, List<String>> wardAreaOptions = {
    'Ward 1': [
      'Ambarkhana',
      'Dargah Mahalla',
      'Darshan Deury',
      'Dargah Gate',
      'Jhornar Par',
      'Mirer Maydan',
      'Mian Fazil Chist',
      'Purba Subidbazar',
      'Rajargali',
    ],
    'Ward 2': [
      'Dariapara',
      'Jallar Par',
      'Kethripara',
      'Kazi Elias',
      'Lama Bazar (Saraspur)',
      'Mirza Jangal',
      'Zindabazar',
    ],
    'Ward 3': [
      'Kajal Shah',
      'Keyapara',
      'Munshipara',
      'Subid Bazar',
      'M.A.G Osmani Medical',
      'Police Line',
    ],
    'Ward 4': [
      'Ambarkhana',
      'Dattapara',
      'Housing Estate',
      'Lichu Bagan',
      'Mazumdari',
      'Monipuri Para',
      'Bonosree',
      'Police Fari',
      'Kamala Bagan',
      'Konapara',
      'Amberkhana Staff Quater',
    ],
    'Ward 5': [
      'Borobazar',
      'Electric Supply',
      'Goypara (Chachnipara)',
      'khasdobir',
      'Eidgah',
      'Hazaribag',
      'T.B colony',
    ],
    'Ward 6': [
      'Badam Bagicha',
      'Choukidighi',
      'Eliaskandi',
      'Syedmogni',
    ],
    'Ward 7': [
      'Jalalabad',
      'West Pir Moholla',
      'Soyef Khan Road',
      'Subid Bazar',
      'Uttar Pir Moholla (Paharika)',
      'Haji Para',
      'Bon kolapara',
      'Fazil Chisht Residential Area',
      'Kolapara',
      'Mitali',
      'Londoni Road',
    ],
    'Ward 8': [
      'Brahman Shashan',
      'Hauldar Para',
      'Kucharpara',
      'Korarpara',
      'Noapara',
      'Ponitula',
      'Pathantula',
    ],
    'Ward 9': [
      'Lake City',
      'Akhalia',
      'Baghbari',
      'Dhanuhata',
      'Kuliapara',
      'Madina Market',
      'Nehari Para',
      'Pathantula',
      'Sagardigir Par',
    ],
    'Ward 10': [
      'Shamimabad',
      'Kanishail',
      'Kalapara',
      'Majumder Para',
      'Molla Para',
      'Nabab Road',
      'Wapda',
    ],
    'Ward 11': [
      'Rohon Das Ratul',
      'Bhatalia',
      'Bil Par',
      'Kajalshah',
      'Kuarpar',
      'Lala Dighirpar',
      'Madhu Shahid',
      'Noapara',
      'Rekabi Baza',
    ],
    'Ward 12': [
      'Bhangatikar',
      'Itakhola',
      'Kuarpar',
      'Saudagartala',
      'Sheikhghat',
    ],
    'Ward 13': [
      'Taltola South',
      'Itakhola',
      'Saudagartala',
      'Tufkhana',
      'Masudhighir Par',
      'Mirja Jungle',
      'Jitu Miar Point',
      'Ramer Dighir Par',
      'Sheikh Para',
      'Lamabazar',
      'Monipuri Rajbari',
    ],
    'Ward 14': [
      'Bandar Bazar',
      'Brahmandi Bazar',
      'Chali Bandar Poschim, Chararpar',
      'Hasan Market',
      'Dak Bangla Road',
      'Dhupra Dighirpar',
      'Jallar Par',
      'Jamtala',
      'Houkers Market',
      'Kastagarh',
      'Kamal Garh',
      'Kalighat',
      'Lal Dighirpar',
      'Paura Biponi',
      'Paura Mirzajangal',
      'Shah Chatt Road',
      'Uttar Taltola',
      'Zinda Bazar',
    ],
    'Ward 15': [
      'Bandar Bazar',
      'Baruth Khana',
      'Chali Bandar',
      'Churi Patti',
      'Hasan Market',
      'Jail Road',
      'Joynagar',
      'Jatarpur',
      'Nayarpool',
      'Noapara',
      'Suphani Ghat',
      'Puran Lane',
      'Uttar Dhopa Dighirpar',
      'Zinda Bazar',
    ],
    'Ward 16': [
      'Charadigirpar',
      'Dhoper Digirpar',
      'Hauapara',
      'Kahan Daura',
      'Kumarpara',
      'Purba Zinda Bazar',
      'Naya Sarak',
      'Saodagor Tola',
      'Tatipara',
    ],
    'Ward 17': [
      'Kazitula',
      'Loharpara',
      'Moktobgoli',
      'Kazi Dighirpar',
      'Electric Supply',
      'Amborkhana',
      'Mirboxtula',
      'Chondontula',
      'Chowhatta',
      'Uchashorok',
      'Kumarpara',
      'Uttor Kazitula',
      'Shahi Eidgah',
      'Noyashorok',
      'Mohole Abdullah',
      'CMB Hills',
    ],
    'Ward 18': [
      'Brajahat Tila',
      'Evergreen',
      'Jhornar Par',
      'Jher jheri Para',
      'Kumar Para',
      'Mira Bazar',
      'Mousumi',
      'Sabuj Bagh',
      'Serak',
      'Shahi Eidgah',
      'Shakhari Para',
    ],
    'Ward 19': [
      'Chandani Tila',
      'Shahi Eidgah',
      'TB Gate',
      'Daptari Para',
      'Darjee Band',
      'Darjee Para',
      'Goner Para',
      'Kahar Para',
      'Raynagar',
      'Sonapara',
    ],
    'Ward 20': [
      'Balichhara South',
      'Kharadi Para',
      'Lama Para',
      'Majumder Para',
      'Roynagar',
      'Senpara',
      'Sonarpara',
      'Shibganj',
      'Shadipur',
      'Tilaghor',
      'Gopaltila',
      'Vatatikor',
    ],
    'Ward 21': [
      'Sonar Para',
      'Bhatatikr',
      'Brahman Para',
      'Lamapara',
      'Hatimbagh',
      'Lakri Para',
      'Sadipur',
      'Shaplabagh',
      'Tilaghar',
      'Rajpara',
    ],
    'Ward 22': [
      'Shahjalal Uposhahar',
      'Uposhohor Block A-J',
      'Shahjalal Uposhohor Bangladesh Bank Colony',
    ],
    'Ward 23': ['Masimpur', 'Mehendibagh', 'Noyagaon'],
    'Ward 24': [
      'Hatimbagh',
      'Kushighat',
      'Lamapara',
      'Mirapara',
      'Sadatikar',
      'Saderpara',
      'Shapla Bagh',
      'Sadipur-2',
      'Tero Ratan',
      'Tultikar',
      'Purbo Sadatikar',
      'Sobujbag',
    ],
    'Ward 25': [
      'Mominkhola',
      'Godrail',
      'Khojarkhola',
      'Barokhola',
      'Musargoan',
      'Lawai',
      'Daudpur',
      'Kajirkhola',
      'Gangu',
    ],
    'Ward 26': [
      'Bharthokhola',
      'Chandnighat',
      'Jalopara',
      'Kadamtali',
    ],
    'Ward 27': [
      'Gotatikor',
      'Alampur',
      'Ganganagar',
      'Patan para',
    ],
  };
  bool isApproved = false;
  List<String> areaOptions = [];
  Future<void> registerUser() async {
    try {
      if (_formKey.currentState!.validate()) {
        FirebaseAuth firebaseAuth = FirebaseAuth.instance;
        final userCredential =
            await firebaseAuth.createUserWithEmailAndPassword(
          email: _emailController.text.trim(),
          password: _confirmPasswordController.text.trim(),
        );

        String userId = userCredential.user!.uid;

        // Get the user ID from the 'userID' collection
        DocumentSnapshot userIdSnapshot = await FirebaseFirestore.instance
            .collection('userID')
            .doc('id')
            .get();
        int currentUserId = userIdSnapshot.get('value');

        // Set the user ID for the signup user
        await FirebaseFirestore.instance.collection('users').doc(userId).set({
          'userId': currentUserId,
          'email': _emailController.text.trim(),
          'name': _nameController.text.trim(),
          'phone': _phoneController.text.trim(),
          'ward': _selectedWard,
          'area': _selectedArea,
          'street': _streetController.text.trim(),
          'houseNo': _houseNoController.text.trim(),
          'floor': _floorController.text.trim(),
          'apartment': _apartmentController.text.trim(),
          'approved': isApproved,
        });

        // Reset the user ID in the 'userID' collection
        await FirebaseFirestore.instance
            .collection('userID')
            .doc('id')
            .update({'value': currentUserId + 1});

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => HomePageHW(),
          ),
        );
      }
    } catch (e) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(
            'Error!',
            style: GoogleFonts.poppins(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.red,
            ),
          ),
          content: Text('Something went wrong!'),
          actions: [
            TextButton(
              child: Text(
                'OK',
                style: GoogleFonts.poppins(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _selectedWard = wardAreaOptions.keys.first;
    areaOptions = wardAreaOptions[_selectedWard]!;
    _selectedArea = areaOptions.first;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text('User Registration'),
        backgroundColor: Colors.green,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '* Required',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 12,
                  ),
                ),
                SizedBox(height: 8.0),
                TextFields(
                  Labeltext: 'Name',
                  isObsecure: false,
                  controller: _nameController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your name';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                Text(
                  '* Required',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 12,
                  ),
                ),
                SizedBox(height: 8.0),
                TextFields(
                  Labeltext: 'Phone Number',
                  isObsecure: false,
                  controller: _phoneController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your phone number';
                    }
                    if (!RegExp(r'^[0-9]{11}$').hasMatch(value)) {
                      return 'Please enter a valid phone number';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                DropdownButtonFormField<String>(
                  value: _selectedWard,
                  decoration: InputDecoration(
                    labelText: 'Ward',
                    labelStyle: TextStyle(color: Colors.grey),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                  items: wardAreaOptions.keys.map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      _selectedWard = newValue!;
                      areaOptions = wardAreaOptions[newValue]!;
                      _selectedArea = areaOptions[0];
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please select the ward';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                DropdownButtonFormField<String>(
                  value: _selectedArea,
                  decoration: InputDecoration(
                    labelText: 'Area',
                    labelStyle: TextStyle(color: Colors.grey),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                  items: areaOptions.map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      _selectedArea = newValue!;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please select the area';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                Text(
                  '* Your road number or Road name',
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 12,
                  ),
                ),
                SizedBox(height: 8.0),
                TextFields(
                  Labeltext: 'Street',
                  isObsecure: false,
                  controller: _streetController,
                  validator: (value) {
                    if (value!.isNotEmpty &&
                        !RegExp(r'^[0-9a-zA-Z\s]+$').hasMatch(value)) {
                      return 'Please enter a valid street';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                Text(
                  '* Required',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 12,
                  ),
                ),
                SizedBox(height: 8.0),
                TextFields(
                  Labeltext: 'House Number',
                  isObsecure: false,
                  controller: _houseNoController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter the house number';
                    }
                    if (!RegExp(r'^[0-9]+$').hasMatch(value)) {
                      return 'Please enter a valid house number.Only number allowed.';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                Text(
                  '* Required',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 12,
                  ),
                ),
                SizedBox(height: 8.0),
                TextFields(
                  Labeltext: 'Floor number',
                  isObsecure: false,
                  controller: _floorController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter the floor';
                    }
                    if (!RegExp(r'^[0-9]+$').hasMatch(value)) {
                      return 'Please enter a valid floor.Only number allowed.';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                Text(
                  '* Required',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 12,
                  ),
                ),
                SizedBox(height: 8.0),
                TextFields(
                  Labeltext: 'Apartment no.',
                  isObsecure: false,
                  controller: _apartmentController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter the apartment';
                    }
                    if (!RegExp(r'^[0-9]+$').hasMatch(value)) {
                      return 'Please enter a valid apartment.Only number allowed.';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                Text(
                  '* Required',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 12,
                  ),
                ),
                SizedBox(height: 8.0),
                TextFields(
                  Labeltext: 'Email',
                  isObsecure: false,
                  controller: _emailController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your email';
                    }
                    if (!RegExp(r'^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$')
                        .hasMatch(value)) {
                      return 'Please enter a valid email address';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFields(
                  Labeltext: 'Password',
                  isObsecure: true,
                  controller: _passwordController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter a password';
                    }
                    if (value.length < 6) {
                      return 'Password should be at least 6 characters';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                Text(
                  '* Required',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 12,
                  ),
                ),
                SizedBox(height: 8.0),
                TextFields(
                  Labeltext: 'Confirm Password',
                  isObsecure: true,
                  controller: _confirmPasswordController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please confirm your password';
                    }
                    if (value != _passwordController.text) {
                      return 'Passwords do not match';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 20),
                Button(text: 'Submit', onTap: registerUser)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
