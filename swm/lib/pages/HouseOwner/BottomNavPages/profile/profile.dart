// ignore_for_file: unnecessary_cast, prefer_const_constructors, library_private_types_in_public_api, deprecated_member_use, prefer_const_literals_to_create_immutables, prefer_interpolation_to_compose_strings, empty_catches, sort_child_properties_last, avoid_print

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:swm/pages/HouseOwner/BottomNavPages/profile/change_address.dart';
import 'package:swm/pages/HouseOwner/BottomNavPages/profile/edit_profile.dart';
import 'dart:io';

import '../../../../component/button.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  File? _image;

  Future<void> _selectImage() async {
    final pickedFile =
        await ImagePicker().getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      setState(() {
        _image = File(pickedFile.path);
      });

      final storageRef = FirebaseStorage.instance.ref().child(
          'profilePictures/${DateTime.now().millisecondsSinceEpoch}.jpg');

      try {
        final uploadTask = storageRef.putFile(_image!);
        final snapshot = await uploadTask.whenComplete(() {});
        if (snapshot.state == TaskState.success) {
          final downloadUrl = await snapshot.ref.getDownloadURL();

          final uid = FirebaseAuth.instance.currentUser!.uid;
          final userRef =
              FirebaseFirestore.instance.collection('users').doc(uid);
          await userRef.update({'profilePicture': downloadUrl});
        }
      } catch (error) {
        print('Error uploading image: $error');
      }
    }
  }

  void sendReminderForApproval(String userId) async {
    final currentUser = FirebaseAuth.instance.currentUser;
    final userName = currentUser?.displayName ?? 'N/A';

    try {
      final reminderDocRef =
          FirebaseFirestore.instance.collection('reminder').doc();
      await reminderDocRef.set({
        'userId': userId,
        'userName': userName,
        'timestamp': FieldValue.serverTimestamp(),
      });
    } catch (error) {
      print('Error sending reminder: $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DocumentSnapshot>(
      stream: FirebaseFirestore.instance
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser!.uid)
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text('Error: ${snapshot.error}'),
          );
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        final userData = snapshot.data!.data() as Map<String, dynamic>;
        final username = userData['name'] ?? 'N/A';
        final area = userData['area'] ?? 'N/A';
        final street = userData['street'] ?? 'N/A';
        final houseNo = userData['houseNo'] ?? 'N/A';
        final userId = userData['userId'].toString();

        final mobile = userData['phone'] ?? 'N/A';
        final profilePicture = userData['profilePicture'] ?? '';
        bool isApproved = userData['approved'] ?? false;

        return Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: _selectImage,
                      child: Stack(
                        alignment: Alignment.bottomRight,
                        children: [
                          CircleAvatar(
                            radius: 64,
                            backgroundColor: Colors.white,
                            foregroundColor: Colors.green,
                            backgroundImage: _image != null
                                ? FileImage(_image!) as ImageProvider<Object>
                                : profilePicture.isNotEmpty
                                    ? NetworkImage(profilePicture)
                                        as ImageProvider<Object>
                                    : AssetImage('lib/images/dustbin.png')
                                        as ImageProvider<Object>,
                          ),
                          IconButton(
                            onPressed: _selectImage,
                            icon: Icon(
                              Icons.camera_alt,
                              color: Colors.green,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 16),
                    Text(
                      username,
                      style: GoogleFonts.poppins(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.green,
                      ),
                    ),
                    SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.location_on,
                          color: Colors.green,
                        ),
                        SizedBox(width: 4),
                        Text(
                          'Address:',
                          style: GoogleFonts.poppins(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            color: Colors.green,
                          ),
                        ),
                      ],
                    ),
                    Text(
                      area + ', ' + street + '/' + houseNo,
                      style: GoogleFonts.poppins(
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.phone,
                          color: Colors.green,
                        ),
                        SizedBox(width: 4),
                        Text(
                          'Mobile:',
                          style: GoogleFonts.poppins(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            color: Colors.green,
                          ),
                        ),
                      ],
                    ),
                    Text(
                      mobile,
                      style: GoogleFonts.poppins(
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(width: 8),
                    Text(
                      'User Id: ' + userId,
                      style: GoogleFonts.poppins(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: Colors.green,
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                        child: !isApproved
                            ? Column(
                                children: [
                                  Text(
                                    'Your account is not approved yet!',
                                    style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.red,
                                    ),
                                  ),
                                  ElevatedButton(
                                    onPressed: () {
                                      sendReminderForApproval(userId);
                                      showDialog(
                                        context: context,
                                        builder: (context) => AlertDialog(
                                          title: Text(
                                            'Reminder sent',
                                            style: GoogleFonts.poppins(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.green,
                                            ),
                                          ),
                                          content: Text(
                                              'Your reminder has been sent!'),
                                          actions: [
                                            TextButton(
                                              onPressed: () =>
                                                  Navigator.pop(context),
                                              child: Text(
                                                'OK',
                                                style: GoogleFonts.poppins(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.green,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                    child: Text('Send Reminder'),
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.green,
                                    ),
                                  ),
                                  SizedBox(height: 32),
                                ],
                              )
                            : Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(Icons.verified),
                                  Text(
                                    ' Verified',
                                    style: GoogleFonts.poppins(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.green,
                                    ),
                                  ),
                                ],
                              )),
                    SizedBox(height: 20),
                    Button(
                      text: 'Settings',
                      onTap: () async {
                        await Navigator.push(
                          context,
                          CupertinoPageRoute(
                            builder: (context) => EditProfilePage(),
                          ),
                        );
                      },
                    ),
                    SizedBox(height: 16),
                    Button(
                      text: 'Change Address',
                      onTap: () {
                        if (isApproved) {
                          Navigator.push(
                            context,
                            CupertinoPageRoute(
                              builder: (context) => AddressChangeRequestPage(),
                            ),
                          );
                        } else {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text(
                                  'Not Verified!',
                                  style: GoogleFonts.poppins(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.red,
                                  ),
                                ),
                                content:
                                    Text('Your account is not approved yet.'),
                                actions: [
                                  TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Text(
                                      'Ok',
                                      style: GoogleFonts.poppins(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.green,
                                      ),
                                    ),
                                  ),
                                  ElevatedButton(
                                    onPressed: () {
                                      sendReminderForApproval(userId);
                                      Navigator.of(context).pop();
                                    },
                                    child: Text('Send Reminder'),
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.green,
                                    ),
                                  ),
                                ],
                              );
                            },
                          );
                        }
                      },
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
