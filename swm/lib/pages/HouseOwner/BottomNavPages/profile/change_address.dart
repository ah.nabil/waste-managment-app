// ignore_for_file: library_private_types_in_public_api, use_build_context_synchronously, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:swm/component/button.dart';

import 'package:swm/component/text_field.dart';

class AddressChangeRequestPage extends StatefulWidget {
  const AddressChangeRequestPage({Key? key}) : super(key: key);

  @override
  _AddressChangeRequestPageState createState() =>
      _AddressChangeRequestPageState();
}

class _AddressChangeRequestPageState extends State<AddressChangeRequestPage> {
  final _formKey = GlobalKey<FormState>();

  String _selectedWard = '';
  String _selectedArea = '';

  final TextEditingController _streetController = TextEditingController();
  final TextEditingController _houseNoController = TextEditingController();
  final TextEditingController _floorController = TextEditingController();
  final TextEditingController _apartmentController = TextEditingController();

  final Map<String, List<String>> wardAreaOptions = {
    'Ward 1': [
      'Ambarkhana',
      'Dargah Mahalla',
      'Darshan Deury',
      'Dargah Gate',
      'Jhornar Par',
      'Mirer Maydan',
      'Mian Fazil Chist',
      'Purba Subidbazar',
      'Rajargali',
    ],
    'Ward 2': [
      'Dariapara',
      'Jallar Par',
      'Kethripara',
      'Kazi Elias',
      'Lama Bazar (Saraspur)',
      'Mirza Jangal',
      'Zindabazar',
    ],
    'Ward 3': [
      'Kajal Shah',
      'Keyapara',
      'Munshipara',
      'Subid Bazar',
      'M.A.G Osmani Medical',
      'Police Line',
    ],
    'Ward 4': [
      'Ambarkhana',
      'Dattapara',
      'Housing Estate',
      'Lichu Bagan',
      'Mazumdari',
      'Monipuri Para',
      'Bonosree',
      'Police Fari',
      'Kamala Bagan',
      'Konapara',
      'Amberkhana Staff Quater',
    ],
    'Ward 5': [
      'Borobazar',
      'Electric Supply',
      'Goypara (Chachnipara)',
      'khasdobir',
      'Eidgah',
      'Hazaribag',
      'T.B colony',
    ],
    'Ward 6': [
      'Badam Bagicha',
      'Choukidighi',
      'Eliaskandi',
      'Syedmogni',
    ],
    'Ward 7': [
      'Jalalabad',
      'West Pir Moholla',
      'Soyef Khan Road',
      'Subid Bazar',
      'Uttar Pir Moholla (Paharika)',
      'Haji Para',
      'Bon kolapara',
      'Fazil Chisht Residential Area',
      'Kolapara',
      'Mitali',
      'Londoni Road',
    ],
    'Ward 8': [
      'Brahman Shashan',
      'Hauldar Para',
      'Kucharpara',
      'Korarpara',
      'Noapara',
      'Ponitula',
      'Pathantula',
    ],
    'Ward 9': [
      'Lake City',
      'Akhalia',
      'Baghbari',
      'Dhanuhata',
      'Kuliapara',
      'Madina Market',
      'Nehari Para',
      'Pathantula',
      'Sagardigir Par',
    ],
    'Ward 10': [
      'Shamimabad',
      'Kanishail',
      'Kalapara',
      'Majumder Para',
      'Molla Para',
      'Nabab Road',
      'Wapda',
    ],
    'Ward 11': [
      'Rohon Das Ratul',
      'Bhatalia',
      'Bil Par',
      'Kajalshah',
      'Kuarpar',
      'Lala Dighirpar',
      'Madhu Shahid',
      'Noapara',
      'Rekabi Baza',
    ],
    'Ward 12': [
      'Bhangatikar',
      'Itakhola',
      'Kuarpar',
      'Saudagartala',
      'Sheikhghat',
    ],
    'Ward 13': [
      'Taltola South',
      'Itakhola',
      'Saudagartala',
      'Tufkhana',
      'Masudhighir Par',
      'Mirja Jungle',
      'Jitu Miar Point',
      'Ramer Dighir Par',
      'Sheikh Para',
      'Lamabazar',
      'Monipuri Rajbari',
    ],
    'Ward 14': [
      'Bandar Bazar',
      'Brahmandi Bazar',
      'Chali Bandar Poschim, Chararpar',
      'Hasan Market',
      'Dak Bangla Road',
      'Dhupra Dighirpar',
      'Jallar Par',
      'Jamtala',
      'Houkers Market',
      'Kastagarh',
      'Kamal Garh',
      'Kalighat',
      'Lal Dighirpar',
      'Paura Biponi',
      'Paura Mirzajangal',
      'Shah Chatt Road',
      'Uttar Taltola',
      'Zinda Bazar',
    ],
    'Ward 15': [
      'Bandar Bazar',
      'Baruth Khana',
      'Chali Bandar',
      'Churi Patti',
      'Hasan Market',
      'Jail Road',
      'Joynagar',
      'Jatarpur',
      'Nayarpool',
      'Noapara',
      'Suphani Ghat',
      'Puran Lane',
      'Uttar Dhopa Dighirpar',
      'Zinda Bazar',
    ],
    'Ward 16': [
      'Charadigirpar',
      'Dhoper Digirpar',
      'Hauapara',
      'Kahan Daura',
      'Kumarpara',
      'Purba Zinda Bazar',
      'Naya Sarak',
      'Saodagor Tola',
      'Tatipara',
    ],
    'Ward 17': [
      'Kazitula',
      'Loharpara',
      'Moktobgoli',
      'Kazi Dighirpar',
      'Electric Supply',
      'Amborkhana',
      'Mirboxtula',
      'Chondontula',
      'Chowhatta',
      'Uchashorok',
      'Kumarpara',
      'Uttor Kazitula',
      'Shahi Eidgah',
      'Noyashorok',
      'Mohole Abdullah',
      'CMB Hills',
    ],
    'Ward 18': [
      'Brajahat Tila',
      'Evergreen',
      'Jhornar Par',
      'Jher jheri Para',
      'Kumar Para',
      'Mira Bazar',
      'Mousumi',
      'Sabuj Bagh',
      'Serak',
      'Shahi Eidgah',
      'Shakhari Para',
    ],
    'Ward 19': [
      'Chandani Tila',
      'Shahi Eidgah',
      'TB Gate',
      'Daptari Para',
      'Darjee Band',
      'Darjee Para',
      'Goner Para',
      'Kahar Para',
      'Raynagar',
      'Sonapara',
    ],
    'Ward 20': [
      'Balichhara South',
      'Kharadi Para',
      'Lama Para',
      'Majumder Para',
      'Roynagar',
      'Senpara',
      'Sonarpara',
      'Shibganj',
      'Shadipur',
      'Tilaghor',
      'Gopaltila',
      'Vatatikor',
    ],
    'Ward 21': [
      'Sonar Para',
      'Bhatatikr',
      'Brahman Para',
      'Lamapara',
      'Hatimbagh',
      'Lakri Para',
      'Sadipur',
      'Shaplabagh',
      'Tilaghar',
      'Rajpara',
    ],
    'Ward 22': [
      'Shahjalal Uposhahar',
      'Uposhohor Block A-J',
      'Shahjalal Uposhohor Bangladesh Bank Colony',
    ],
    'Ward 23': ['Masimpur', 'Mehendibagh', 'Noyagaon'],
    'Ward 24': [
      'Hatimbagh',
      'Kushighat',
      'Lamapara',
      'Mirapara',
      'Sadatikar',
      'Saderpara',
      'Shapla Bagh',
      'Sadipur-2',
      'Tero Ratan',
      'Tultikar',
      'Purbo Sadatikar',
      'Sobujbag',
    ],
    'Ward 25': [
      'Mominkhola',
      'Godrail',
      'Khojarkhola',
      'Barokhola',
      'Musargoan',
      'Lawai',
      'Daudpur',
      'Kajirkhola',
      'Gangu',
    ],
    'Ward 26': [
      'Bharthokhola',
      'Chandnighat',
      'Jalopara',
      'Kadamtali',
    ],
    'Ward 27': [
      'Gotatikor',
      'Alampur',
      'Ganganagar',
      'Patan para',
    ],
  };

  List<String> areaOptions = [];
  Future<void> changeAddress() async {
    if (_formKey.currentState!.validate()) {
      try {
        final userId = FirebaseAuth.instance.currentUser!.uid;

        // Set the user ID for the signup user
        await FirebaseFirestore.instance
            .collection('address_change_req')
            .doc(userId)
            .set({
          'ward': _selectedWard,
          'area': _selectedArea,
          'street': _streetController.text.trim(),
          'houseNo': _houseNoController.text.trim(),
          'floor': _floorController.text.trim(),
          'apartment': _apartmentController.text.trim(),
        });

        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text(
              'Success',
              style: GoogleFonts.poppins(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.green,
              ),
            ),
            content: Text('Your request has been sent.'),
            actions: [
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: Text(
                  'OK',
                  style: GoogleFonts.poppins(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                ),
              ),
            ],
          ),
        );
      } catch (e) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text(
              'Error!',
              style: GoogleFonts.poppins(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.red,
              ),
            ),
            content: Text('Something went wrong!'),
            actions: [
              TextButton(
                child: Text(
                  'OK',
                  style: GoogleFonts.poppins(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        );
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _selectedWard = wardAreaOptions.keys.first;
    areaOptions = wardAreaOptions[_selectedWard]!;
    _selectedArea = areaOptions.first;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green, // Set the app bar color to green
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text('Address Change Request'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 16.0),
                DropdownButtonFormField<String>(
                  value: _selectedWard,
                  decoration: InputDecoration(
                    labelText: 'Ward',
                    labelStyle: TextStyle(color: Colors.grey),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green, width: 2.0),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                  items: wardAreaOptions.keys
                      .map((String ward) => DropdownMenuItem<String>(
                            value: ward,
                            child: Text(ward),
                          ))
                      .toList(),
                  onChanged: (String? value) {
                    if (value != null) {
                      setState(() {
                        _selectedWard = value;
                        areaOptions = wardAreaOptions[value]!;
                        _selectedArea = areaOptions.first;
                      });
                    }
                  },
                ),
                SizedBox(height: 16.0),
                DropdownButtonFormField<String>(
                  value: _selectedArea,
                  decoration: InputDecoration(
                    labelText: 'Area',
                    labelStyle: TextStyle(color: Colors.grey),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green, width: 2.0),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                  items: areaOptions
                      .map((String area) => DropdownMenuItem<String>(
                            value: area,
                            child: Text(area),
                          ))
                      .toList(),
                  onChanged: (String? value) {
                    if (value != null) {
                      setState(() {
                        _selectedArea = value;
                      });
                    }
                  },
                ),
                SizedBox(height: 16.0),
                TextFields(
                  Labeltext: 'Street',
                  isObsecure: false,
                  controller: _streetController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter the street';
                    }
                    if (!RegExp(r'^[0-9a-zA-Z\s]+$').hasMatch(value)) {
                      return 'Please enter a valid street';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFields(
                  Labeltext: 'House Number',
                  isObsecure: false,
                  controller: _houseNoController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter the house number';
                    }
                    if (!RegExp(r'^[0-9]+$').hasMatch(value)) {
                      return 'Please enter a valid house number';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFields(
                  Labeltext: 'Floor',
                  isObsecure: false,
                  controller: _floorController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter the floor';
                    }
                    if (!RegExp(r'^[0-9]+$').hasMatch(value)) {
                      return 'Please enter a valid floor';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFields(
                  Labeltext: 'Apartment',
                  isObsecure: false,
                  controller: _apartmentController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter the apartment';
                    }
                    if (!RegExp(r'^[0-9]+$').hasMatch(value)) {
                      return 'Please enter a valid apartment';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                Button(
                  onTap: changeAddress,
                  text: 'Request Changes',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
