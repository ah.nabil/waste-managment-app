// ignore_for_file: use_build_context_synchronously, prefer_const_constructors, use_key_in_widget_constructors

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../component/button.dart';

class PaymentNotifier extends ChangeNotifier {
  final TextEditingController amountController = TextEditingController();
  final TextEditingController collectorIdController = TextEditingController();
  final FirebaseFirestore firestore = FirebaseFirestore.instance;

  String userId = '';
  bool isPaymentConfirmed = true;
  bool isPaymentInProgress = false;
  bool isApproved = false;

  final formKey = GlobalKey<FormState>();

  void getCurrentUserId() async {
    final user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      final uid = user.uid;
      // Replace 'users' with the actual collection name for users
      // Assuming you have a document with the current user's ID as the document ID
      DocumentSnapshot userSnapshot =
          await FirebaseFirestore.instance.collection('users').doc(uid).get();

      if (userSnapshot.exists) {
        userId = userSnapshot['userId'].toString();
        isApproved = userSnapshot['approved'] ?? false;
      }
    }
  }

  void storePaymentData(BuildContext context) async {
    if (formKey.currentState!.validate()) {
      final String amount = amountController.text;
      final String collectorId = collectorIdController.text;
      final DateTime paymentTimestamp = DateTime.now();

      isPaymentInProgress = true;
      notifyListeners();

      try {
        await firestore.collection('payments').add({
          'amount': amount,
          'collectorId': collectorId,
          'timestamp': paymentTimestamp,
          'userId': userId,
          'confirmed': false,
        });

        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                'Payment Successful',
                style: GoogleFonts.poppins(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
              content: Text('The payment has been recorded.'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'OK',
                    style: GoogleFonts.poppins(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.green,
                    ),
                  ),
                ),
              ],
            );
          },
        );

        isPaymentInProgress = false;
        isPaymentConfirmed = false;
        notifyListeners();
        formKey.currentState!.reset();

        amountController.clear();
        collectorIdController.clear();
      } catch (e) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                'Error!',
                style: GoogleFonts.poppins(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.red,
                ),
              ),
              content: Text('An error occurred while recording the payment.'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'OK',
                    style: GoogleFonts.poppins(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.green,
                    ),
                  ),
                ),
              ],
            );
          },
        );

        isPaymentInProgress = false;
        notifyListeners();
      }
    }
  }

  void cancelPayment(BuildContext context) async {
    isPaymentInProgress = true;
    notifyListeners();

    try {
      final QuerySnapshot snapshot = await firestore
          .collection('payments')
          .where('userId', isEqualTo: userId)
          .where('confirmed', isEqualTo: false)
          .limit(1)
          .get();

      if (snapshot.docs.isNotEmpty) {
        final paymentId = snapshot.docs[0].id;
        await firestore.collection('payments').doc(paymentId).delete();

        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                'Payment Canceled!',
                style: GoogleFonts.poppins(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
              content: Text('The payment has been canceled.'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'OK',
                    style: GoogleFonts.poppins(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.green,
                    ),
                  ),
                ),
              ],
            );
          },
        );
      } else {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Error'),
              content: Text('No pending payment found.'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('OK'),
                ),
              ],
            );
          },
        );
      }

      isPaymentInProgress = false;
      isPaymentConfirmed = true;
      notifyListeners();
    } catch (e) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error'),
            content: Text('An error occurred while canceling the payment.'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('OK'),
              ),
            ],
          );
        },
      );

      isPaymentInProgress = false;
      notifyListeners();
    }
  }
}

class PaymentPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ref) {
    final paymentNotifier = ref.watch(paymentNotifierProvider);

    paymentNotifier.getCurrentUserId();

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(24.0),
          child: Form(
            key: paymentNotifier.formKey,
            child: Column(
              children: [
                Text(
                  'Enter Amount and Waste collector\'s Id',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  controller: paymentNotifier.amountController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Amount',
                    labelStyle: TextStyle(color: Colors.grey),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green, width: 2.0),
                        borderRadius: BorderRadius.circular(20)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green, width: 2.0),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                  enabled: paymentNotifier.isPaymentConfirmed,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter the amount';
                    }
                    if (!RegExp(r'^[0-9]+$').hasMatch(value)) {
                      return 'Please enter a valid amount';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  controller: paymentNotifier.collectorIdController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: "Waste Collector's ID",
                    labelStyle: TextStyle(color: Colors.grey),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green, width: 2.0),
                        borderRadius: BorderRadius.circular(20)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green, width: 2.0),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                  enabled: paymentNotifier.isPaymentConfirmed,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter Waste Collector\'s ID';
                    }
                    if (!RegExp(r'^[0-9]+$').hasMatch(value)) {
                      return 'Please enter a valid ID';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                Text(
                  paymentNotifier.userId.isNotEmpty
                      ? 'Current User ID: ${paymentNotifier.userId}'
                      : 'Loading User ID...',
                  style: TextStyle(fontSize: 16.0),
                ),
                SizedBox(height: 32.0),
                Button(
                  text: paymentNotifier.isPaymentConfirmed
                      ? 'Update Payment'
                      : 'Cancel Payment',
                  onTap: paymentNotifier.isPaymentConfirmed
                      ? () => paymentNotifier.storePaymentData(context)
                      : () => paymentNotifier.cancelPayment(context),
                ),
                SizedBox(height: 16.0),
                StreamBuilder<QuerySnapshot>(
                  stream: FirebaseFirestore.instance
                      .collection('payments')
                      .where('userId', isEqualTo: paymentNotifier.userId)
                      .where('confirmed', isEqualTo: false)
                      .snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.hasData) {
                      final payments = snapshot.data!.docs;
                      if (payments.isNotEmpty) {
                        return Column(
                          children: payments.map((payment) {
                            final amount = payment['amount'];
                            final collectorId = payment['collectorId'];
                            final time =
                                DateFormat('yyyy-MM-dd HH:mm:ss').format(
                              payment['timestamp'].toDate(),
                            );

                            return Container(
                              margin: EdgeInsets.symmetric(vertical: 8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Payment Amount: $amount',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text('Collector ID: $collectorId'),
                                  Text('Time: $time'),
                                  Divider(),
                                  Text(
                                    'Payment Not Confirmed',
                                    style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              ),
                            );
                          }).toList(),
                        );
                      }
                    }
                    return Container();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

final paymentNotifierProvider = ChangeNotifierProvider<PaymentNotifier>(
  (ref) => PaymentNotifier(),
);
