// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SchedulePage extends StatelessWidget {
  const SchedulePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: const EdgeInsets.only(left: 82),
          child: Text(
            'Schedule',
            style: TextStyle(color: Colors.green),
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance
            .collection('schedule')
            .doc('EQCAlrRraG6djeR4NuBD')
            .collection('cards')
            .snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text('Error loading schedule data'),
            );
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
            return Center(
              child: Text('No schedule data found'),
            );
          }

          var scheduleCards = snapshot.data!.docs;

          return ListView.builder(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
            itemCount: scheduleCards.length,
            itemBuilder: (context, index) {
              var scheduleCard = scheduleCards[index];
              var title = scheduleCard['title'];
              var time = scheduleCard['time'];
              bool isScheduled = scheduleCard['isScheduled'];

              return Column(
                children: [
                  ScheduleCard(
                    title: title,
                    time: time,
                    isScheduled: isScheduled,
                  ),
                  SizedBox(height: 16),
                ],
              );
            },
          );
        },
      ),
    );
  }
}

class ScheduleCard extends StatelessWidget {
  final String title;
  final String time;
  final bool isScheduled;

  const ScheduleCard({
    Key? key,
    required this.title,
    required this.time,
    this.isScheduled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      color: isScheduled ? Colors.green[50] : Colors.white,
      child: ListTile(
        title: Text(
          title,
          style: GoogleFonts.poppins(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: isScheduled ? Colors.green : Colors.black,
          ),
        ),
        subtitle: Text(
          time,
          style: GoogleFonts.poppins(
            fontSize: 16,
            color: isScheduled ? Colors.green : Colors.black87,
          ),
        ),
        trailing: Icon(
          isScheduled ? Icons.check_circle : Icons.cancel,
          color: isScheduled ? Colors.green : Colors.red,
          size: 24,
        ),
      ),
    );
  }
}
