// ignore_for_file: use_build_context_synchronously, prefer_const_constructors, use_key_in_widget_constructors, library_private_types_in_public_api

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class CommunityPage extends StatefulWidget {
  @override
  _CommunityPageState createState() => _CommunityPageState();
}

class _CommunityPageState extends State<CommunityPage> {
  late CollectionReference postsCollection;
  final TextEditingController _postController = TextEditingController();

  @override
  void initState() {
    super.initState();
    postsCollection = FirebaseFirestore.instance.collection('posts');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<QuerySnapshot>(
        stream:
            postsCollection.orderBy('timestamp', descending: true).snapshots(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasData) {
            final posts = snapshot.data!.docs
                .map((doc) => Post.fromSnapshot(doc))
                .toList();
            return ListView.builder(
              itemCount: posts.length,
              itemBuilder: (context, index) {
                final post = posts[index];
                return FutureBuilder<String?>(
                  future: getUserName(post.userId),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return CircularProgressIndicator();
                    } else if (snapshot.hasData) {
                      post.userName = snapshot.data!;
                      final currentUser = FirebaseAuth.instance.currentUser;
                      final isCurrentUserOwner =
                          post.userId == currentUser?.uid;
                      return PostWidget(
                        post: post,
                        onCommentPressed: () {
                          showCommentDialog(context, post);
                        },
                        onDeletePressed: () {
                          deletePost(post);
                        },
                        onDeleteCommentPressed: (commentIndex) {
                          deleteComment(post, commentIndex);
                        },
                        isCurrentUserOwner: isCurrentUserOwner,
                      );
                    } else if (snapshot.hasError) {
                      return Text('Error loading username: ${snapshot.error}');
                    } else {
                      return Text('Username not found');
                    }
                  },
                );
              },
            );
          } else if (snapshot.hasError) {
            return Text('Error fetching posts: ${snapshot.error}');
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green,
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () {
          showPostDialog(context);
        },
      ),
    );
  }

  void showPostDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        String newPostText = '';

        return AlertDialog(
          title: Text(
            'New Post',
            style: GoogleFonts.poppins(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.green,
            ),
          ),
          content: TextField(
            controller: _postController,
            onChanged: (value) {
              newPostText = value;
            },
            decoration: InputDecoration(hintText: 'Enter your post'),
          ),
          actions: [
            TextButton(
              onPressed: () async {
                if (newPostText.isNotEmpty) {
                  final userName =
                      await getUserName(FirebaseAuth.instance.currentUser!.uid);
                  await postsCollection.add({
                    'text': newPostText,
                    'timestamp': FieldValue.serverTimestamp(),
                    'comments': [],
                    'userId': FirebaseAuth.instance.currentUser!.uid,
                    'userName': userName,
                  });
                }
                _postController.clear();
                Navigator.of(context).pop();
              },
              child: Text(
                'Post',
                style: GoogleFonts.poppins(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  void showCommentDialog(BuildContext context, Post post) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        String newCommentText = '';

        return AlertDialog(
          title: Text(
            'New Comment',
            style: GoogleFonts.poppins(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.green,
            ),
          ),
          content: TextField(
            onChanged: (value) {
              newCommentText = value;
            },
            decoration: InputDecoration(hintText: 'Enter your comment'),
          ),
          actions: [
            TextButton(
              onPressed: () async {
                if (newCommentText.isNotEmpty) {
                  final updatedComments = List<String>.from(post.comments)
                    ..add(newCommentText);
                  await postsCollection
                      .doc(post.id)
                      .update({'comments': updatedComments});
                }
                Navigator.of(context).pop();
              },
              child: Text(
                'Comment',
                style: GoogleFonts.poppins(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  void deletePost(Post post) async {
    await postsCollection.doc(post.id).delete();
  }

  void deleteComment(Post post, int commentIndex) async {
    final updatedComments = List<String>.from(post.comments)
      ..removeAt(commentIndex);
    await postsCollection.doc(post.id).update({'comments': updatedComments});
  }
}

Future<String> getUserName(String userId) async {
  final snapshot =
      await FirebaseFirestore.instance.collection('users').doc(userId).get();

  final userData = snapshot.data() as Map<String, dynamic>;
  final username = userData['name'] ?? 'N/A';
  return username;
}

class Post {
  final String id;
  String text;
  final List<String> comments;
  final String userId;
  String userName;
  final Timestamp timestamp; // Add the timestamp property

  Post({
    required this.id,
    required this.text,
    required this.comments,
    required this.userId,
    required this.userName,
    required this.timestamp,
  });

  factory Post.fromSnapshot(DocumentSnapshot snapshot) {
    final data = snapshot.data() as Map<String, dynamic>;
    final text = data['text'] ?? '';
    final comments = List<String>.from(data['comments'] ?? []);
    final userId = data['userId'] ?? '';
    final userName = data['userName'] ?? '';
    final timestamp = data['timestamp'] as Timestamp?;

    return Post(
      id: snapshot.id,
      text: text,
      comments: comments,
      userId: userId,
      userName: userName,
      timestamp: timestamp ?? Timestamp.now(),
    );
  }
}

class PostWidget extends StatefulWidget {
  final Post post;
  final VoidCallback onCommentPressed;
  final VoidCallback onDeletePressed;
  final Function(int) onDeleteCommentPressed;
  final bool isCurrentUserOwner;

  const PostWidget({
    required this.post,
    required this.onCommentPressed,
    required this.onDeletePressed,
    required this.onDeleteCommentPressed,
    required this.isCurrentUserOwner,
  });

  @override
  _PostWidgetState createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget> {
  late TextEditingController _editPostController;

  @override
  void initState() {
    super.initState();
    _editPostController = TextEditingController(text: widget.post.text);
  }

  @override
  void dispose() {
    _editPostController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Color.fromARGB(255, 236, 250, 236),
      margin: EdgeInsets.all(10),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  widget.post.userName,
                  style: GoogleFonts.poppins(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  getPostTime(widget.post.timestamp),
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    color: Colors.green,
                  ),
                ),
              ],
            ),
            SizedBox(height: 5),
            Text(
              widget.post.text,
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 10),
            Row(
              children: [
                TextButton(
                  onPressed: widget.onCommentPressed,
                  child: Text('Comment'),
                ),
                SizedBox(width: 10),
                Text('${widget.post.comments.length} comments'),
                SizedBox(width: 10),
                if (widget.isCurrentUserOwner)
                  Row(
                    children: [
                      IconButton(
                        onPressed: widget.onDeletePressed,
                        icon: Icon(Icons.delete),
                        color: Colors.red,
                      ),
                      IconButton(
                        onPressed: showEditDialog,
                        icon: Icon(Icons.edit),
                        color: Colors.green,
                      ),
                    ],
                  ),
              ],
            ),
            SizedBox(height: 10),
            if (widget.post.comments.isNotEmpty)
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Comments:',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 5),
                  ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: widget.post.comments.length,
                    itemBuilder: (context, index) {
                      return Row(
                        children: [
                          Text('- ${widget.post.comments[index]}'),
                          if (widget.isCurrentUserOwner)
                            IconButton(
                              onPressed: () =>
                                  widget.onDeleteCommentPressed(index),
                              icon: Icon(Icons.delete),
                              color: Colors.red,
                            ),
                        ],
                      );
                    },
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  void showEditDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        String updatedText = widget.post.text;

        return AlertDialog(
          title: Text(
            'Edit Post',
            style: GoogleFonts.poppins(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.green,
            ),
          ),
          content: TextField(
            controller: _editPostController,
            onChanged: (value) {
              setState(() {
                updatedText = value;
              });
            },
            decoration: InputDecoration(hintText: 'Enter your updated post'),
          ),
          actions: [
            TextButton(
              onPressed: () async {
                if (updatedText.isNotEmpty) {
                  await FirebaseFirestore.instance
                      .collection('posts')
                      .doc(widget.post.id)
                      .update({'text': updatedText});
                  setState(() {
                    widget.post.text = updatedText;
                  });
                }
                Navigator.of(context).pop();
              },
              child: Text(
                'Update',
                style: GoogleFonts.poppins(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  String getPostTime(Timestamp timestamp) {
    final dateTime = timestamp.toDate();
    final timeFormat = DateFormat('hh:mm a, dd-MM-yyyy');
    return timeFormat.format(dateTime);
  }
}
