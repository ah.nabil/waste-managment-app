// ignore_for_file: prefer_const_constructors, library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:swm/component/button.dart';
import 'package:swm/component/text_field.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Apply extends StatefulWidget {
  const Apply({Key? key}) : super(key: key);

  @override
  _ApplyState createState() => _ApplyState();
}

class _ApplyState extends State<Apply> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _dobController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _nationalIDController = TextEditingController();
  final TextEditingController _workExperienceController =
      TextEditingController();
  final TextEditingController _educationController = TextEditingController();
  final TextEditingController _availabilityController = TextEditingController();

  void _submitForm() {
    if (_formKey.currentState!.validate()) {
      // Handle form submission logic
      String name = _nameController.text;
      String dob = _dobController.text;
      String phone = _phoneController.text;
      String email = _emailController.text;
      String nationalID = _nationalIDController.text;
      String workExperience = _workExperienceController.text;
      String education = _educationController.text;
      String availability = _availabilityController.text;

      // Store the information in Firestore
      FirebaseFirestore firestore = FirebaseFirestore.instance;
      firestore.collection('wcapply').add({
        'name': name,
        'dob': dob,
        'phone': phone,
        'email': email,
        'nationalID': nationalID,
        'workExperience': workExperience,
        'education': education,
        'availability': availability,
      }).then((value) {
        // Data successfully stored in Firestore
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text(
              'Success',
              style: GoogleFonts.poppins(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.green,
              ),
            ),
            content: Text('Your application has been submitted.'),
            actions: [
              TextButton(
                child: Text(
                  'OK',
                  style: GoogleFonts.poppins(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context); // Go back to the previous screen
                },
              ),
            ],
          ),
        );
      }).catchError((error) {
        // Error occurred while storing data in Firestore
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text(
              'Error!',
              style: GoogleFonts.poppins(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.red,
              ),
            ),
            content:
                Text('An error occurred while submitting your application.'),
            actions: [
              TextButton(
                child: Text(
                  'OK',
                  style: GoogleFonts.poppins(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text('Apply for approval'),
        backgroundColor: Colors.green,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '* Required',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 12,
                  ),
                ),
                SizedBox(height: 8.0),
                TextFields(
                  controller: _nameController,
                  Labeltext: 'Full Name',
                  isObsecure: false,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your name';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16),
                TextFields(
                  controller: _dobController,
                  Labeltext: 'Date of Birth',
                  isObsecure: false,
                ),
                SizedBox(height: 16),
                Text(
                  '* Required',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 12,
                  ),
                ),
                SizedBox(height: 8.0),
                TextFields(
                  controller: _phoneController,
                  Labeltext: 'Phone Number',
                  isObsecure: false,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your phone number';
                    }
                    if (!RegExp(r'^[0-9]{11}$').hasMatch(value)) {
                      return 'Please enter a valid phone number';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16),
                Text(
                  '* Required',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 12,
                  ),
                ),
                SizedBox(height: 8.0),
                TextFields(
                  controller: _emailController,
                  Labeltext: 'Email Address',
                  isObsecure: false,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your email';
                    }
                    if (!RegExp(r'^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$')
                        .hasMatch(value)) {
                      return 'Please enter a valid email address';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16),
                Text(
                  '* Required',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 12,
                  ),
                ),
                SizedBox(height: 8.0),
                TextFields(
                  controller: _nationalIDController,
                  Labeltext: 'National ID or Social Security Number',
                  isObsecure: false,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter the NID number';
                    }
                    if (!RegExp(r'^[0-9]+$').hasMatch(value)) {
                      return 'Please enter a valid NID number';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16),
                TextFields(
                  controller: _workExperienceController,
                  Labeltext: 'Work Experience (if any)',
                  isObsecure: false,
                ),
                SizedBox(height: 16),
                TextFields(
                  controller: _educationController,
                  Labeltext: 'Education Level',
                  isObsecure: false,
                ),
                SizedBox(height: 16),
                TextFields(
                  controller: _availabilityController,
                  Labeltext: 'Availability and Schedule',
                  isObsecure: false,
                ),
                SizedBox(height: 24),
                Button(text: 'Apply', onTap: _submitForm),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
