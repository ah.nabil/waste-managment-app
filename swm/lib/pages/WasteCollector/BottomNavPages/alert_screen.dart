// ignore_for_file: prefer_const_declarations, unnecessary_cast, avoid_print, prefer_const_constructors, use_build_context_synchronously, prefer_const_constructors_in_immutables, library_private_types_in_public_api

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:http/http.dart' as http;

import '../../../component/button.dart';

class SendAlertPage extends StatefulWidget {
  SendAlertPage({Key? key}) : super(key: key);

  @override
  _SendAlertPageState createState() => _SendAlertPageState();
}

class _SendAlertPageState extends State<SendAlertPage> {
  String selectedWard = '';
  String selectedArea = '';
  final Map<String, List<String>> wardAreaOptions = {
    'Ward 1': [
      'Ambarkhana',
      'Dargah Mahalla',
      'Darshan Deury',
      'Dargah Gate',
      'Jhornar Par',
      'Mirer Maydan',
      'Mian Fazil Chist',
      'Purba Subidbazar',
      'Rajargali',
    ],
    'Ward 2': [
      'Dariapara',
      'Jallar Par',
      'Kethripara',
      'Kazi Elias',
      'Lama Bazar (Saraspur)',
      'Mirza Jangal',
      'Zindabazar',
    ],
    'Ward 3': [
      'Kajal Shah',
      'Keyapara',
      'Munshipara',
      'Subid Bazar',
      'M.A.G Osmani Medical',
      'Police Line',
    ],
    'Ward 4': [
      'Ambarkhana',
      'Dattapara',
      'Housing Estate',
      'Lichu Bagan',
      'Mazumdari',
      'Monipuri Para',
      'Bonosree',
      'Police Fari',
      'Kamala Bagan',
      'Konapara',
      'Amberkhana Staff Quater',
    ],
    'Ward 5': [
      'Borobazar',
      'Electric Supply',
      'Goypara (Chachnipara)',
      'khasdobir',
      'Eidgah',
      'Hazaribag',
      'T.B colony',
    ],
    'Ward 6': [
      'Badam Bagicha',
      'Choukidighi',
      'Eliaskandi',
      'Syedmogni',
    ],
    'Ward 7': [
      'Jalalabad',
      'West Pir Moholla',
      'Soyef Khan Road',
      'Subid Bazar',
      'Uttar Pir Moholla (Paharika)',
      'Haji Para',
      'Bon kolapara',
      'Fazil Chisht Residential Area',
      'Kolapara',
      'Mitali',
      'Londoni Road',
    ],
    'Ward 8': [
      'Brahman Shashan',
      'Hauldar Para',
      'Kucharpara',
      'Korarpara',
      'Noapara',
      'Ponitula',
      'Pathantula',
    ],
    'Ward 9': [
      'Lake City',
      'Akhalia',
      'Baghbari',
      'Dhanuhata',
      'Kuliapara',
      'Madina Market',
      'Nehari Para',
      'Pathantula',
      'Sagardigir Par',
    ],
    'Ward 10': [
      'Shamimabad',
      'Kanishail',
      'Kalapara',
      'Majumder Para',
      'Molla Para',
      'Nabab Road',
      'Wapda',
    ],
    'Ward 11': [
      'Rohon Das Ratul',
      'Bhatalia',
      'Bil Par',
      'Kajalshah',
      'Kuarpar',
      'Lala Dighirpar',
      'Madhu Shahid',
      'Noapara',
      'Rekabi Baza',
    ],
    'Ward 12': [
      'Bhangatikar',
      'Itakhola',
      'Kuarpar',
      'Saudagartala',
      'Sheikhghat',
    ],
    'Ward 13': [
      'Taltola South',
      'Itakhola',
      'Saudagartala',
      'Tufkhana',
      'Masudhighir Par',
      'Mirja Jungle',
      'Jitu Miar Point',
      'Ramer Dighir Par',
      'Sheikh Para',
      'Lamabazar',
      'Monipuri Rajbari',
    ],
    'Ward 14': [
      'Bandar Bazar',
      'Brahmandi Bazar',
      'Chali Bandar Poschim, Chararpar',
      'Hasan Market',
      'Dak Bangla Road',
      'Dhupra Dighirpar',
      'Jallar Par',
      'Jamtala',
      'Houkers Market',
      'Kastagarh',
      'Kamal Garh',
      'Kalighat',
      'Lal Dighirpar',
      'Paura Biponi',
      'Paura Mirzajangal',
      'Shah Chatt Road',
      'Uttar Taltola',
      'Zinda Bazar',
    ],
    'Ward 15': [
      'Bandar Bazar',
      'Baruth Khana',
      'Chali Bandar',
      'Churi Patti',
      'Hasan Market',
      'Jail Road',
      'Joynagar',
      'Jatarpur',
      'Nayarpool',
      'Noapara',
      'Suphani Ghat',
      'Puran Lane',
      'Uttar Dhopa Dighirpar',
      'Zinda Bazar',
    ],
    'Ward 16': [
      'Charadigirpar',
      'Dhoper Digirpar',
      'Hauapara',
      'Kahan Daura',
      'Kumarpara',
      'Purba Zinda Bazar',
      'Naya Sarak',
      'Saodagor Tola',
      'Tatipara',
    ],
    'Ward 17': [
      'Kazitula',
      'Loharpara',
      'Moktobgoli',
      'Kazi Dighirpar',
      'Electric Supply',
      'Amborkhana',
      'Mirboxtula',
      'Chondontula',
      'Chowhatta',
      'Uchashorok',
      'Kumarpara',
      'Uttor Kazitula',
      'Shahi Eidgah',
      'Noyashorok',
      'Mohole Abdullah',
      'CMB Hills',
    ],
    'Ward 18': [
      'Brajahat Tila',
      'Evergreen',
      'Jhornar Par',
      'Jher jheri Para',
      'Kumar Para',
      'Mira Bazar',
      'Mousumi',
      'Sabuj Bagh',
      'Serak',
      'Shahi Eidgah',
      'Shakhari Para',
    ],
    'Ward 19': [
      'Chandani Tila',
      'Shahi Eidgah',
      'TB Gate',
      'Daptari Para',
      'Darjee Band',
      'Darjee Para',
      'Goner Para',
      'Kahar Para',
      'Raynagar',
      'Sonapara',
    ],
    'Ward 20': [
      'Balichhara South',
      'Kharadi Para',
      'Lama Para',
      'Majumder Para',
      'Roynagar',
      'Senpara',
      'Sonarpara',
      'Shibganj',
      'Shadipur',
      'Tilaghor',
      'Gopaltila',
      'Vatatikor',
    ],
    'Ward 21': [
      'Sonar Para',
      'Bhatatikr',
      'Brahman Para',
      'Lamapara',
      'Hatimbagh',
      'Lakri Para',
      'Sadipur',
      'Shaplabagh',
      'Tilaghar',
      'Rajpara',
    ],
    'Ward 22': [
      'Shahjalal Uposhahar',
      'Uposhohor Block A-J',
      'Shahjalal Uposhohor Bangladesh Bank Colony',
    ],
    'Ward 23': ['Masimpur', 'Mehendibagh', 'Noyagaon'],
    'Ward 24': [
      'Hatimbagh',
      'Kushighat',
      'Lamapara',
      'Mirapara',
      'Sadatikar',
      'Saderpara',
      'Shapla Bagh',
      'Sadipur-2',
      'Tero Ratan',
      'Tultikar',
      'Purbo Sadatikar',
      'Sobujbag',
    ],
    'Ward 25': [
      'Mominkhola',
      'Godrail',
      'Khojarkhola',
      'Barokhola',
      'Musargoan',
      'Lawai',
      'Daudpur',
      'Kajirkhola',
      'Gangu',
    ],
    'Ward 26': [
      'Bharthokhola',
      'Chandnighat',
      'Jalopara',
      'Kadamtali',
    ],
    'Ward 27': [
      'Gotatikor',
      'Alampur',
      'Ganganagar',
      'Patan para',
    ],
  };

  List<String> areaOptions = [];
  @override
  void initState() {
    super.initState();
    selectedWard = wardAreaOptions.keys.first;
    areaOptions = wardAreaOptions[selectedWard]!;
    selectedArea = areaOptions.first;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 24, left: 24, right: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Please select ward and area to send alert',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.w600,
                    color: Colors.green,
                    fontFamily: GoogleFonts.poppins().fontFamily),
              ),
              const SizedBox(
                height: 24,
              ),
              DropdownButtonFormField<String>(
                value: selectedWard,
                decoration: InputDecoration(
                  labelText: 'Ward',
                  labelStyle: TextStyle(color: Colors.grey),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.green, width: 2.0),
                      borderRadius: BorderRadius.circular(20)),
                ),
                items: wardAreaOptions.keys
                    .map((String ward) => DropdownMenuItem<String>(
                          value: ward,
                          child: Text(ward),
                        ))
                    .toList(),
                onChanged: (String? value) {
                  if (value != null) {
                    setState(() {
                      selectedWard = value;
                      areaOptions = wardAreaOptions[value]!;
                      selectedArea = areaOptions.first;
                    });
                  }
                },
              ),
              SizedBox(height: 16.0),
              DropdownButtonFormField<String>(
                value: selectedArea,
                decoration: InputDecoration(
                  labelText: 'Area',
                  labelStyle: TextStyle(color: Colors.grey),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.green, width: 2.0),
                      borderRadius: BorderRadius.circular(20)),
                ),
                items: areaOptions
                    .map((String area) => DropdownMenuItem<String>(
                          value: area,
                          child: Text(area),
                        ))
                    .toList(),
                onChanged: (String? value) {
                  if (value != null) {
                    setState(() {
                      selectedArea = value;
                    });
                  }
                },
              ),
              const SizedBox(height: 32),
              Button(
                text: 'Send Alert!',
                onTap: _sendAlert,
              ),
            ],
          ),
        ),
      ),
    );
  }

  _sendAlert() async {
    final String title = 'Waste collection alert!'; // Fixed title

    // Send a push notification to the users in the selected area
    final querySnapshot = await FirebaseFirestore.instance
        .collection('users')
        .where('ward', isEqualTo: selectedWard)
        .where('area', isEqualTo: selectedArea)
        .get();

    for (final doc in querySnapshot.docs) {
      final userData = doc.data() as Map<String, dynamic>;
      final deviceToken = userData['deviceToken'];

      try {
        await http.post(
          Uri.parse('https://fcm.googleapis.com/fcm/send'),
          headers: <String, String>{
            'content-type': 'application/json',
            'Authorization':
                'key=AAAANwEWj6g:APA91bFYMz5q90d0Wpps5ErF9wIPCbh6lHKAD19cN8bTR_TGbXCFG8wryCi2avHgnYDwknGBm1Vj0Wm0jO__3WkqxiApoKuPsU6pnJrCdonnwK0V3zZOGrFmp-dQDTqphzM81IhHhSgP' // 'key=YOUR_SERVER_KEY'
          },
          body: jsonEncode(<String, dynamic>{
            'priority': 'high',
            'data': <String, dynamic>{
              'click_action': 'FLUTTER_NOTIFICATION_CLICK',
              'status': 'done',
              'body': 'I am here to pick up your waste',
              'title': title,
            },
            "notification": <String, dynamic>{
              "title": title,
              "body": 'I am here to pick up your waste',
            },
            "to": deviceToken,
          }),
        );
      } catch (e) {
        print('Error sending notification: $e');
      }
    }

    // Show confirmation dialog or navigate back to the previous page
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
          'Alert Sent',
          style: GoogleFonts.poppins(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.green,
          ),
        ),
        content: Text('The alert has been sent successfully.'),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text(
              'OK',
              style: GoogleFonts.poppins(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: Colors.green,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
