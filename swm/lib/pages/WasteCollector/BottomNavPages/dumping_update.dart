// ignore_for_file: prefer_const_constructors, use_build_context_synchronously, library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../../component/button.dart';

class DumpingUpdatePage extends StatefulWidget {
  const DumpingUpdatePage({super.key});

  @override
  _DumpingUpdatePageState createState() => _DumpingUpdatePageState();
}

class _DumpingUpdatePageState extends State<DumpingUpdatePage> {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  List<String> dumpingLocationOptions = [
    'Madina Market',
    'Rikabi Bazar',
    'Tilaghor',
    'Upashahar',
    'Mejortila',
  ];
  int selectedDumpingLocationIndex = 0;
  DateTime selectedDateTime = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: EdgeInsets.all(24.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 50),
                Text(
                  'Select dumping location',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                ),
                SizedBox(height: 16.0),
                DropdownButtonFormField<int>(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green, width: 2.0),
                        borderRadius: BorderRadius.circular(20)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green, width: 2.0),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                  value: selectedDumpingLocationIndex,
                  items: dumpingLocationOptions
                      .asMap()
                      .entries
                      .map(
                        (entry) => DropdownMenuItem<int>(
                          value: entry.key,
                          child: Text(entry.value),
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      selectedDumpingLocationIndex = value!;
                    });
                  },
                ),
                SizedBox(height: 16.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Select Date and Time: ',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.green,
                      ),
                    ),
                    TextButton(
                      onPressed: () async {
                        // Show date picker and update selectedDateTime
                        DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2015),
                          lastDate: DateTime(2100),
                        );

                        if (pickedDate != null) {
                          TimeOfDay? pickedTime = await showTimePicker(
                            context: context,
                            initialTime: TimeOfDay.now(),
                          );

                          if (pickedTime != null) {
                            setState(() {
                              selectedDateTime = DateTime(
                                pickedDate.year,
                                pickedDate.month,
                                pickedDate.day,
                                pickedTime.hour,
                                pickedTime.minute,
                              );
                            });
                          }
                        }
                      },
                      child: Text(
                        DateFormat('yyyy-MM-dd HH:mm').format(selectedDateTime),
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.blue,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 16.0),
                Button(
                  text: 'Submit Update',
                  onTap: () => updateDumpingStatus(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void updateDumpingStatus() async {
    try {
      final User? user = _auth.currentUser;
      if (user != null) {
        final String wcId = await getWasteCollectorId();
        final String dumpingLocation =
            dumpingLocationOptions[selectedDumpingLocationIndex];
        final Timestamp timestamp = Timestamp.fromDate(selectedDateTime);

        await _firestore.collection('dumping_updates').add({
          'wcId': wcId,
          'dumpingLocation': dumpingLocation,
          'timestamp': timestamp,
        });

        // Clear the text field after submitting
        setState(() {
          selectedDumpingLocationIndex = 0;
        });

        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                'Dumping Update',
                style: GoogleFonts.poppins(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
              content: Text('Dumping update submitted successfully.'),
              actions: [
                TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text(
                    'OK',
                    style: GoogleFonts.poppins(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.green,
                    ),
                  ),
                ),
              ],
            );
          },
        );
      }
    } catch (e) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error'),
            content: Text('Failed to submit dumping update.'),
            actions: [
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: Text('OK'),
              ),
            ],
          );
        },
      );
    }
  }

  Future<String> getWasteCollectorId() async {
    final QuerySnapshot wcInfoSnapshot =
        await _firestore.collection('wcinfo').limit(1).get();

    if (wcInfoSnapshot.docs.isNotEmpty) {
      final wcInfo = wcInfoSnapshot.docs.first.data() as Map<String, dynamic>;
      return wcInfo['wcId'];
    }

    throw Exception('Waste collector ID not found');
  }
}
