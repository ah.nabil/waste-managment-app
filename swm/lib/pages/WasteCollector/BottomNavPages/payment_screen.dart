// ignore_for_file: prefer_const_constructors, library_private_types_in_public_api, use_key_in_widget_constructors, empty_catches

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';

class PaymentConfirmationPage extends StatefulWidget {
  @override
  _PaymentConfirmationPageState createState() =>
      _PaymentConfirmationPageState();
}

class _PaymentConfirmationPageState extends State<PaymentConfirmationPage> {
  String? collectorId;
  Map<String, bool> paymentConfirmationStatus = {};
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  @override
  void initState() {
    super.initState();
    getCollectorId();
    checkPaymentConfirmation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (collectorId != null)
                Center(
                  child: Text(
                    'Waste Collector ID: $collectorId',
                    style: TextStyle(
                        color: Colors.green,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              SizedBox(height: 16.0),
              Text(
                'Payment Details:',
                style: TextStyle(
                    color: Colors.green,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 8.0),
              StreamBuilder<QuerySnapshot>(
                stream: _firestore
                    .collection('payments')
                    .where('collectorId', isEqualTo: collectorId)
                    .where('confirmed', isEqualTo: false)
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  }

                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return CircularProgressIndicator();
                  }

                  if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
                    return Text('Payment not found');
                  }

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children:
                        snapshot.data!.docs.map((DocumentSnapshot document) {
                      final paymentData =
                          document.data() as Map<String, dynamic>;
                      final paymentId = document.id;
                      final amount = paymentData['amount'] ?? '';
                      final collectorId = paymentData['collectorId'] ?? '';
                      final senderId = paymentData['userId'] ?? '';
                      final time = DateFormat('yyyy-MM-dd HH:mm:ss').format(
                        paymentData['timestamp'].toDate(),
                      );

                      final paymentConfirmed =
                          paymentConfirmationStatus[paymentId] ?? false;

                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Amount: $amount tk'),
                          Text('Collector ID: $collectorId'),
                          Text('Sender ID: $senderId'),
                          Text('Time:  $time'),
                          SizedBox(height: 8.0),
                          if (paymentConfirmed)
                            Text(
                              'Payment has been confirmed.',
                              style: TextStyle(
                                  color: Colors.green,
                                  fontWeight: FontWeight.bold),
                            )
                          else
                            ElevatedButton(
                              onPressed: () => confirmPayment(paymentId),
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors
                                    .green, // Set the button color to green
                              ),
                              child: Text(
                                'Confirm Payment',
                                style: TextStyle(
                                  color: Colors
                                      .white, // Set the text color to white
                                ),
                              ),
                            ),
                          SizedBox(height: 16.0),
                        ],
                      );
                    }).toList(),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void getCollectorId() async {
    try {
      final wcInfoSnapshot =
          await _firestore.collection('wcinfo').limit(1).get();
      if (wcInfoSnapshot.docs.isNotEmpty) {
        final wcInfo = wcInfoSnapshot.docs.first.data();
        setState(() {
          collectorId = wcInfo['wcId'];
        });
      }
    } catch (e) {}
  }

  void checkPaymentConfirmation() async {
    try {
      final paymentSnapshot = await _firestore
          .collection('payments')
          .where('collectorId', isEqualTo: collectorId)
          .get();

      for (final DocumentSnapshot document in paymentSnapshot.docs) {
        final paymentId = document.id;
        final paymentData = document.data() as Map<String, dynamic>;
        final confirmed = paymentData['confirmed'] ?? false;

        setState(() {
          paymentConfirmationStatus[paymentId] = confirmed;
        });
      }
    } catch (e) {}
  }

  void confirmPayment(String paymentId) async {
    try {
      await _firestore
          .collection('payments')
          .doc(paymentId)
          .update({'confirmed': true});

      setState(() {
        paymentConfirmationStatus[paymentId] = true;
      });
    } catch (e) {}
  }
}
