// ignore_for_file: library_private_types_in_public_api, file_names, prefer_const_constructors, use_build_context_synchronously, no_leading_underscores_for_local_identifiers

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:swm/pages/WasteCollector/BottomNavPages/dumping_update.dart';
import 'package:swm/pages/WasteCollector/BottomNavPages/alert_screen.dart';
import 'package:swm/pages/WasteCollector/BottomNavPages/payment_screen.dart';
import 'package:swm/pages/WasteCollector/BottomNavPages/schedule_screen.dart';
import 'package:swm/pages/WasteCollector/HomePage/notification_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class HomePageWC extends StatefulWidget {
  const HomePageWC({Key? key}) : super(key: key);

  @override
  _HomePageWCState createState() => _HomePageWCState();
}

class _HomePageWCState extends State<HomePageWC> {
  String userName = '';
  String userEmail = '';
  int _currentIndex = 0;
  List<Widget> navpages = [
    SchedulePage(),
    SendAlertPage(),
    PaymentConfirmationPage(),
    DumpingUpdatePage()
  ];

  Future<bool> _onWillPop() async {
    if (_currentIndex == 0) {
      // If the current index is 0 (first index),
      // allow the back button to pop the screen
      return false;
    } else {
      // Otherwise, navigate to the previous index
      setState(() {
        _currentIndex = _currentIndex - 1;
      });
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DocumentSnapshot>(
      stream: FirebaseFirestore.instance
          .collection('wcinfo')
          .doc(FirebaseAuth.instance.currentUser!.uid)
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text('Error: ${snapshot.error}'),
          );
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        final userData = snapshot.data!.data() as Map<String, dynamic>;
        final userEmail = userData['email'] ?? '';
        final userName = userData['name'] ?? '';
        return WillPopScope(
          onWillPop: _onWillPop,
          child: Scaffold(
            appBar: AppBar(
              title: Center(child: Text('Hello, $userName!')),
              actions: [
                IconButton(
                  icon: Icon(Icons.notifications),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NotificationPage()));
                  },
                ),
              ],
              backgroundColor: Colors.green, // Set app bar color to green
            ),
            drawer: Drawer(
              child: Column(
                children: [
                  UserAccountsDrawerHeader(
                    accountName: Text(
                      userName,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    accountEmail: Text(
                      userEmail,
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                    currentAccountPicture: CircleAvatar(
                      backgroundImage: AssetImage('lib/images/dustbin.png'),
                      backgroundColor: Colors.white,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.green,
                    ),
                  ),

                  ListTile(
                    leading: Icon(Icons.report),
                    title: Text('Report'),
                    onTap: () {
                      Navigator.pushNamed(context, '/report');
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.info),
                    title: Text('About'),
                    onTap: () {
                      Navigator.pushNamed(context, '/about');
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.logout),
                    title: Text('Logout'),
                    onTap: () async {
                      final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
                      await _firebaseAuth.signOut();
                      Navigator.pushReplacementNamed(context, '/home');
                    },
                  ),
                  Spacer(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 60,
                        width: 120,
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(10)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Smart',
                            style: TextStyle(
                                fontSize: 32.0,
                                fontWeight: FontWeight.w600,
                                color: Colors.white,
                                fontFamily: GoogleFonts.poppins().fontFamily),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Text(
                        'City',
                        style: TextStyle(
                            fontSize: 32.0,
                            fontWeight: FontWeight.w600,
                            color: Colors.green,
                            fontFamily: GoogleFonts.poppins().fontFamily),
                      ),
                    ],
                  ), // Add spacer to push items to the bottom
                  Divider(), // Add divider below the last item
                ],
              ),
            ),
            body: navpages[_currentIndex],
            bottomNavigationBar: BottomNavigationBar(
              currentIndex: _currentIndex,
              onTap: (index) {
                setState(() {
                  _currentIndex = index;
                });
              },
              selectedItemColor:
                  Colors.green, // Set selected item color to green
              unselectedItemColor:
                  Colors.black, // Set unselected item color to black
              items: const [
                BottomNavigationBarItem(
                  icon: Icon(Icons.schedule),
                  label: 'Schedule',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.alarm_add_rounded),
                  label: 'Send Alert',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.payment),
                  label: 'Payment',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.update),
                  label: 'Update',
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
