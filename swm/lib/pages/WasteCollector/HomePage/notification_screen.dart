import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class NotificationPage extends StatelessWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Notifications'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.green, // Set app bar color to green
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 24),
        children: const [
          NotificationItem(
            title: 'New Collection Request',
            description: 'A new waste collection request has been received.',
            time: '9:30 AM',
            isRead: false,
          ),
          SizedBox(height: 16),
          NotificationItem(
            title: 'Collection Reminder',
            description: 'Reminder: Waste collection scheduled for today.',
            time: 'Yesterday',
            isRead: true,
          ),
          SizedBox(height: 16),
          // Add more NotificationItem widgets for each notification
        ],
      ),
    );
  }
}

class NotificationItem extends StatelessWidget {
  final String title;
  final String description;
  final String time;
  final bool isRead;

  const NotificationItem({
    super.key,
    required this.title,
    required this.description,
    required this.time,
    required this.isRead,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: isRead ? Colors.grey[200] : Colors.white,
        borderRadius: BorderRadius.circular(8),
      ),
      child: ListTile(
        contentPadding: const EdgeInsets.all(12),
        title: Text(
          title,
          style: GoogleFonts.poppins(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: isRead ? Colors.black : Colors.green,
          ),
        ),
        subtitle: Text(
          description,
          style: GoogleFonts.poppins(fontSize: 16),
        ),
        trailing: Text(
          time,
          style: GoogleFonts.poppins(
            fontSize: 14,
            color: isRead ? Colors.grey : Colors.green,
          ),
        ),
        onTap: () {},
      ),
    );
  }
}
