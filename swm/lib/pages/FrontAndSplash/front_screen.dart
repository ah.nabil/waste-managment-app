// ignore_for_file: prefer_const_constructors, file_names

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:swm/component/button.dart';

// ignore: use_key_in_widget_constructors
class FrontPage extends StatefulWidget {
  @override
  // ignore: library_private_types_in_public_api
  _FrontPageState createState() => _FrontPageState();
}

class _FrontPageState extends State<FrontPage> {
  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 24, right: 24, bottom: 100),
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              //mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 120),
                Image.asset(
                  'lib/images/dustbin.png',
                ),
                SizedBox(
                  height: 64,
                ),
                Text(
                  'Choose your option?',
                  style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w300,
                      color: Color.fromARGB(255, 185, 184, 184),
                      fontFamily: GoogleFonts.poppins().fontFamily),
                ),
                SizedBox(
                  height: 20,
                ),
                Button(
                    text: 'Waste Collector',
                    onTap: () => Navigator.pushNamed(context, '/loginWC')),
                SizedBox(
                  height: 24,
                ),
                Button(
                    text: 'House Owner',
                    onTap: () => Navigator.pushNamed(context, '/loginHW'))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
