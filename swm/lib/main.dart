// ignore_for_file: prefer_const_constructors

import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:swm/pages/HouseOwner/drawerPages/extra_pickup.dart';
import 'package:swm/pages/CommonDrawerPage/about.dart';
import 'package:swm/pages/HouseOwner/drawerPages/report.dart';
import 'package:swm/pages/HouseOwner/HomePage/notification.dart';
import 'package:swm/pages/HouseOwner/auth/signUp.dart';
import 'package:swm/pages/WasteCollector/auth/apply.dart';
import 'package:swm/pages/WasteCollector/HomePage/homeScreen_WC.dart';
import 'package:swm/pages/FrontAndSplash/front_screen.dart';
import 'package:swm/pages/HouseOwner/auth/login_hw.dart';
import 'package:swm/pages/WasteCollector/auth/login_wc.dart';
import 'package:swm/pages/FrontAndSplash/splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

FutureOr<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
    apiKey: 'AIzaSyCVPenecHHfWSKofSmSPCw4hN38f-P5WWg',
    appId: '1:236241457064:android:654c520fa0d4fe921b179d',
    messagingSenderId: '236241457064',
    projectId: 'smart-city-68b45',
  ));
  FirebaseMessaging messaging = FirebaseMessaging.instance;
  NotificationSettings settings = await messaging.requestPermission();
  if (settings.authorizationStatus == AuthorizationStatus.authorized) {
  } else {}
  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'My App',
        initialRoute: '/splash',
        routes: {
          '/splash': (context) => SplashScreen(),
          '/home': (context) => FrontPage(),
          '/loginWC': (context) => LoginWC(),
          '/loginHW': (context) => LoginHW(),
          '/signupHW': (context) => SignUpHW(),
          '/apply': (context) => Apply(),
          '/notifications': (context) => NotificationPage(),
          '/report': (context) => ReportPage(),
          '/extraPickup': (context) => RequestExtraPickupPage(),
          '/about': (context) => AboutPage(),
          '/homeWC': (context) => HomePageWC()
        });
  }
}
